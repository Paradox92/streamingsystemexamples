package hfu.wwc;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.state.WindowStore;

import java.util.Properties;

import static java.time.Duration.ofSeconds;
import static java.util.Arrays.asList;
import static org.apache.kafka.streams.kstream.Suppressed.BufferConfig.unbounded;
import static org.apache.kafka.streams.kstream.Suppressed.untilWindowCloses;
import static org.apache.kafka.streams.kstream.TimeWindows.of;

public class WindowedWordCount {

  public static void appendOutput(KStream<String, Long> stream, String outputTopicName) {
    stream.to(outputTopicName, Produced.with(Serdes.String(), Serdes.Long()));
  }

  public static KStream<String, String> createInput(StreamsBuilder builder, String inputTopicName) {
    return builder.stream(inputTopicName, Consumed.with(Serdes.String(), Serdes.String()));
  }

  public static KStream<String, Long> buildWindowedTopology(KStream<String, String> inputStream){
    return inputStream
        .filter((key, value) -> !value.isEmpty())
        .flatMapValues(value -> asList(value.split("\\W+")))
        .groupBy((key, value) -> value)
        .windowedBy(of(ofSeconds(5))
            .advanceBy(ofSeconds(3))
            .grace(ofSeconds(2)))
        .count(Materialized.<String, Long, WindowStore<Bytes, byte[]>>as("counts-store").withRetention(ofSeconds(7))
            .withKeySerde(Serdes.String())
            .withValueSerde(Serdes.Long())
        )
        .suppress(untilWindowCloses(unbounded()))
        .toStream()
        .map((key, value) -> new KeyValue<>(key.key(), value));
  }

  public static Properties createProperties(String applicationId, String bootstrapServer) {
    Properties props = new Properties();
    props.put(StreamsConfig.APPLICATION_ID_CONFIG, applicationId);
    props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
    props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
    props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());

    return props;
  }
}
