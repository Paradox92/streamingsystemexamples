package hfu.wwc;

import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.kstream.*;
import org.junit.Test;

import java.time.Duration;
import java.time.Instant;
import java.util.*;

import static java.time.Duration.ofSeconds;
import static org.junit.Assert.*;

public class WordCountTest {

    //<editor-fold desc="Configuration">
    String applicationId = "windowed-word-count"; // EngineConfig.name
    String bootstrapServer = "localhost:9002"; // auto generated
    //</editor-fold>

    @Test
    public void testWindowing() {

        //<editor-fold desc="Create Topology">
        Properties props = WindowedWordCount.createProperties(
            applicationId, bootstrapServer); // PipelineOptions.path (PropsRef-Pipeline)

        final StreamsBuilder builder = new StreamsBuilder(); // generated

        String inputTopicName = "input";  // EventSource.name
        KStream stream = builder.stream( // EventSourceRef (InputRef-EventSourceRef)
            inputTopicName,
            Consumed.with(Serdes.String(), Serdes.String())); // EventSource.coder

        KStream topologyStream = WindowedWordCount.buildWindowedTopology(stream); // EventSink.path
        String outputTopicName = "output"; // EventSink.name
        topologyStream.to(outputTopicName,
            Produced.with(Serdes.String(), Serdes.Long())); // EventSink.coder

        final Topology topology = builder.build(); // generated (TopoRef-Pipeline)
        //</editor-fold>

        try (TopologyTestDriver driver = new TopologyTestDriver(topology, props)) { // generated per topology

            //<editor-fold desc="Test Sources">
            TestInputTopic inputWords = driver.createInputTopic(inputTopicName,
                new StringSerializer(), new StringSerializer()
            ); // EventSource (InputRef-EventSource) 
            //</editor-fold>

            //<editor-fold desc="Test Sinks">
            TestOutputTopic outputWordCount = driver.createOutputTopic(outputTopicName,
                new StringDeserializer(), new LongDeserializer()
            ); // EventSink
            //</editor-fold>

            //<editor-fold desc="Test Inputs">
            Instant now = Instant.now();
            driver.getTimestampedWindowStore("windowStore");

            inputWords.pipeInput(inputTopicName, "a b c", now);
            inputWords.pipeInput(inputTopicName, "a c d c", now.plus(ofSeconds(6)));
            inputWords.pipeInput(inputTopicName, "d a d", now.plus(ofSeconds(3)));

            inputWords.pipeInput(inputTopicName, " ", now.plus(ofSeconds(9)));
            inputWords.pipeInput(inputTopicName, "a b b a", now.plus(ofSeconds(4)));

            inputWords.pipeInput(inputTopicName, "EXIT", now.plus(Duration.ofDays(1)));
            //</editor-fold>

            //<editor-fold desc="Validation">
            assertTrue("Output contains specified values",
                outputWordCount.readKeyValuesToList().containsAll(
                    new ArrayList(){{
                        add(new KeyValue<>("a", 1L));
                        add(new KeyValue<>("a", 4L));
                        add(new KeyValue<>("b", 2L));
                        add(new KeyValue<>("c", 2L));
                        add(new KeyValue<>("d", 3L));
                    }}
            ));
            //</editor-fold>
        }
    }
}
