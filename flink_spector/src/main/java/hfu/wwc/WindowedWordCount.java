package hfu.wwc;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.windowing.assigners.SlidingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.util.Collector;

public class WindowedWordCount {
  public static DataStream<Tuple2<String, Integer>> windowedWordCountStream(DataStream<String> stream) {
    return stream
        .flatMap(new Tokenizer())
        .keyBy(0)
        .window(SlidingEventTimeWindows.of(Time.seconds(5), Time.seconds(3)))
        .allowedLateness(Time.seconds(2))
        .sum(1);
  }

  public static final class Tokenizer implements FlatMapFunction<String, Tuple2<String, Integer>> {
    @Override
    public void flatMap(String value, Collector<Tuple2<String, Integer>> out) {
      String[] tokens = value.toLowerCase().split("\\W+");

      for (String token : tokens) {
        if (token.length() > 0) {
          out.collect(new Tuple2<String, Integer>(token, 1));
        }
      }
    }
  }
}
