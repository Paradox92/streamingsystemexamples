package hfu.wwc;

import io.flinkspector.core.collection.ExpectedRecords;
import io.flinkspector.datastream.DataStreamTestBase;
import io.flinkspector.datastream.input.EventTimeInput;
import io.flinkspector.datastream.input.EventTimeInputBuilder;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.junit.Test;

public class WindowingTest extends DataStreamTestBase {

    //<editor-fold desc="Configuration">
    //</editor-fold>

    @Test
    public void testWindowing() {

        //<editor-fold desc="Test Inputs">
        EventTimeInput<String> source =
                EventTimeInputBuilder.startWith("a b c")
                    .emit("a c d c", after(6, seconds))
                    .emit("d a d", before(3, seconds))
                    .emit("", after(6, seconds)) // dummy to increase processing time
                    .emit("a b b a", before(5, seconds));
        //</editor-fold>
        
        //<editor-fold desc="Test Sources">
        DataStream<String> dataStream = createTestStream(source);
        //</editor-fold>

        //<editor-fold desc="Create Pipeline">
        //</editor-fold>

        //<editor-fold desc="Test Sinks">
        DataStream<Tuple2<String, Integer>> wordStream = WindowedWordCount.windowedWordCountStream(dataStream);
        //</editor-fold>

        //<editor-fold desc="Validation">
        ExpectedRecords<Tuple2<String, Integer>> expected =
                new ExpectedRecords<Tuple2<String, Integer>>()
                        // first window
                        .expect(Tuple2.of("a", 1), 2)
                        .expect(Tuple2.of("b", 1), 2)
                        .expect(Tuple2.of("c", 1), 2)
                        // second window
                        .expect(Tuple2.of("a", 2), 1)
                        //.expect(Tuple2.of("b", 1), 1)
                        //.expect(Tuple2.of("c", 1), 1)
                        .expect(Tuple2.of("d", 2), 1)
                        // third window
                        .expect(Tuple2.of("a", 4), 1)
                        .expect(Tuple2.of("b", 2), 1)
                        .expect(Tuple2.of("c", 2), 2)
                        .expect(Tuple2.of("d", 3), 1)
                        // fourth window
                        //.expect(Tuple2.of("a", 1), 1)
                        //.expect(Tuple2.of("c", 2), 1)
                        .expect(Tuple2.of("d", 1), 1);

        assertStream(wordStream, expected.refine().sameFrequency());
        //</editor-fold>
    }
}
