package hfu.wwc;

import junit.framework.TestCase;
import org.apache.beam.runners.direct.DirectRunner;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.coders.AvroCoder;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.testing.PAssert;
import org.apache.beam.sdk.testing.TestStream;
import org.apache.beam.sdk.transforms.windowing.BoundedWindow;
import org.apache.beam.sdk.transforms.windowing.IntervalWindow;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;
import org.joda.time.Duration;
import org.joda.time.Instant;
import org.junit.Test;

public class WindowedWordCountTest extends TestCase {
 
    //<editor-fold desc="Configuration">
    //</editor-fold>

    @Test
    public void test_windowedWordCountTest() {

        //<editor-fold desc="Create Pipeline">
        PipelineOptions options = WindowedWordCount.getOptions();
        options.setRunner(DirectRunner.class);

        Pipeline pipeline = Pipeline.create(options);
        //</editor-fold>

        //<editor-fold desc="Test Sources">
        TestStream.Builder<LineWithTimestamp> source =
            TestStream.create(AvroCoder.of(LineWithTimestamp.class));
        //</editor-fold>

        //<editor-fold desc="Test Inputs">
        Instant now = new Instant(0);
        TestStream<LineWithTimestamp> testInput = source
                .addElements(new LineWithTimestamp("a b c", now))
                .advanceWatermarkTo(now.plus(Duration.standardSeconds(6)))
                .addElements(
                        new LineWithTimestamp("a c d c", now.plus(Duration.standardSeconds(6))),
                        new LineWithTimestamp("d a d", now.plus(Duration.standardSeconds(3)))
                ).advanceWatermarkTo(now.plus(Duration.standardSeconds(9)))
                .addElements(new LineWithTimestamp("a b b a", now.plus(Duration.standardSeconds(4))))
                .advanceWatermarkToInfinity();

        PCollection<LineWithTimestamp> lines = pipeline.apply(testInput);
        //</editor-fold>

        //<editor-fold desc="Test Sinks">
        PCollection wordCounts = WindowedWordCount.createPipeline(lines);
        //</editor-fold>

        //<editor-fold desc="Validation">
        BoundedWindow firstWindow = new IntervalWindow(
                now.minus(Duration.standardSeconds(3)), now.plus(Duration.standardSeconds(2)));
        BoundedWindow secondWindow = new IntervalWindow(now, now.plus(Duration.standardSeconds(5)));
        BoundedWindow thirdWindow = new IntervalWindow(
                now.plus(Duration.standardSeconds(3)), now.plus(Duration.standardSeconds(8)));
        BoundedWindow fourthWindow = new IntervalWindow(
                now.plus(Duration.standardSeconds(6)), now.plus(Duration.standardSeconds(11)));

        PAssert.that("First Window as expected", wordCounts)
                .inOnTimePane(firstWindow)
                .containsInAnyOrder(
                        KV.of("a", 1L),
                        KV.of("b", 1L),
                        KV.of("c", 1L)
                );

        PAssert.that("'a b c' on time, 'd a d' late and 'a b b a' too late in second window", wordCounts)
                .inWindow(secondWindow)
                .containsInAnyOrder(
                        KV.of("a", 1L),
                        KV.of("b", 1L),
                        KV.of("c", 1L),
                        KV.of("a", 2L),
                        KV.of("d", 2L)
                );

        PAssert.that("'d a d' and 'a c d c' on time and 'a b b a' late in third window", wordCounts)
                .inWindow(thirdWindow)
                .containsInAnyOrder(
                        KV.of("a", 2L),
                        KV.of("c", 2L),
                        KV.of("d", 3L),
                        KV.of("a", 4L),
                        KV.of("b", 2L)
                );

        PAssert.that("'a c d c' on time in fourth window", wordCounts)
                .inWindow(fourthWindow)
                .containsInAnyOrder(
                        KV.of("a", 1L),
                        KV.of("c", 2L),
                        KV.of("d", 1L)
                );

        PAssert.that("'a b b a' late in third pane", wordCounts)
                .inLatePane(thirdWindow)
                .containsInAnyOrder(
                        KV.of("a", 4L),
                        KV.of("b", 2L)
                );


        pipeline.run().waitUntilFinish();
        //</editor-fold>
    }
}
