package hfu.wwc;

import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.Count;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.transforms.windowing.SlidingWindows;
import org.apache.beam.sdk.transforms.windowing.Window;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;
import org.joda.time.Duration;

public class WindowedWordCount {
    public static PCollection<KV<String, Long>> createPipeline(PCollection<LineWithTimestamp> pipeline) {
        return pipeline
                .apply(ParDo.of(new SplitLinesFn()))
                .apply(Window.<String>into(
                        SlidingWindows.of(Duration.standardSeconds(5)).every(Duration.standardSeconds(3)))
                        .accumulatingFiredPanes()
                        .withAllowedLateness(Duration.standardSeconds(2)))
                .apply(Count.perElement());
    }

    private static class SplitLinesFn extends DoFn<LineWithTimestamp, String> {
        @ProcessElement
        public void processElement(@Element LineWithTimestamp lineWithTs, OutputReceiver<String> receiver) {
            for (String word :lineWithTs.content.split(" ")) {
                receiver.outputWithTimestamp(word, lineWithTs.timestamp);
            }
        }

        @Override
        public Duration getAllowedTimestampSkew() {
            return Duration.ZERO.plus(Long.MAX_VALUE);
        }
    }

    public static PipelineOptions getOptions() {
        PipelineOptions options = PipelineOptionsFactory.create();
        options.setJobName("Windowed Word Count");

        return options;
    }
}
