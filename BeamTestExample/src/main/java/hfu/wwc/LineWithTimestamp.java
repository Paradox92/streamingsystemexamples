package hfu.wwc;

import org.apache.beam.sdk.coders.AvroCoder;
import org.apache.beam.sdk.coders.DefaultCoder;
import org.joda.time.Instant;

@DefaultCoder(AvroCoder.class)
public class LineWithTimestamp {

    public String content;
    public Instant timestamp;

    public LineWithTimestamp() {}

    public LineWithTimestamp(String line, Instant timestamp) {
        this.content = line;
        this.timestamp = timestamp;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}