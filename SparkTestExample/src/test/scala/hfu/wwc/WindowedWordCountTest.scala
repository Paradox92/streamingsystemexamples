package hfu.wwc

import java.sql.Timestamp.from
import java.time.Duration.ofSeconds
import java.time.Instant

import org.apache.spark.sql._
import org.apache.spark.sql.execution.streaming.{LongOffset, MemoryStream}
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.streaming.{OutputMode, StreamingQuery}
import org.scalatest.{FunSuite, Matchers}

class WindowedWordCountTest extends FunSuite with Matchers {

  //<editor-fold desc="Configuration">
  System.setProperty("hadoop.home.dir", "C:\\hadoop-2.8.1")

  val spark : SparkSession = SparkSession.builder().
      appName("WindowedWordCountTest").
      master("local[*]").getOrCreate()
  implicit val sqlCtx: SQLContext = spark.sqlContext
  import spark.implicits._
  //</editor-fold>

  test("Windows must be filled") {

    //<editor-fold desc="Test Sources">
    val linesStream: MemoryStream[LineWithTimestamp] = MemoryStream[LineWithTimestamp]
    val linesSource: Dataset[LineWithTimestamp] = linesStream.toDS // to data set
    //</editor-fold>

    //<editor-fold desc="Create Topology">
    //</editor-fold>

    //<editor-fold desc="Test Sinks">
    val frame: DataFrame =
      WindowedWordCount.windowedWordCountFrame(linesSource, spark)

    val streamingQuery: StreamingQuery = frame
        .writeStream // generated
        .format("memory") // generated
        .queryName("output")  // EventSink.name
        .option("checkpointLocation", "data\\checkpoint") // generated
        .outputMode(OutputMode.Update()) // generated
        .start // generated
    //</editor-fold>

    //<editor-fold desc="Test Inputs">
    val now = Instant.ofEpochMilli(0)
    val batch = Seq( // Processes
      LineWithTimestamp("a b c", from(now)),
      LineWithTimestamp("d a d", from(now.plus(ofSeconds(3)))),
      LineWithTimestamp("a b b a", from(now.plus(ofSeconds(4)))),
      LineWithTimestamp("a c d c", from(now.plus(ofSeconds(6))))
    )
    val currentOffset = linesStream.addData(batch) // generated
    streamingQuery.processAllAvailable() // generated
    linesStream.commit(currentOffset.asInstanceOf[LongOffset]) // generated
    //</editor-fold>

    //<editor-fold desc="Test Validation">
    var resultFrame: DataFrame = spark. // SparkRef
      table("output") // EventSink.name

    resultFrame = resultFrame.drop("window") // user defined

    val results = resultFrame.collect

    exactly(2, results) shouldBe Row("c", 2)
    atLeast(1, results) shouldBe Row("b", 2)
    atMost(1, results) shouldBe Row("a", 4)
    //</editor-fold>
  }
}
