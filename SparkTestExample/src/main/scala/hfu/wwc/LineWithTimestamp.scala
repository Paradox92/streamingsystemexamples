package hfu.wwc

import java.sql.Timestamp

case class LineWithTimestamp(line: String, timestamp: Timestamp)
