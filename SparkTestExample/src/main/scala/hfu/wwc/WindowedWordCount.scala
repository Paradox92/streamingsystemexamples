package hfu.wwc

import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}
import org.apache.spark.sql.functions.{explode, split, window}
import org.apache.spark.sql.streaming.{OutputMode, StreamingQuery}

object WindowedWordCount {
  def windowedWordCountFrame(session: Dataset[LineWithTimestamp], spark: SparkSession): DataFrame = {
    val sqlContext = spark.sqlContext
    import sqlContext.implicits._

    session.
      withColumn("line", explode(split($"line", " "))).
      withWatermark("timestamp", "1 seconds").
      groupBy(window(
        $"timestamp", "5 seconds", "3 seconds"),
        $"line").
      count()
  }

  def streamingQuery(session: DataFrame, queryName: String) : StreamingQuery = {
    session
      .writeStream
      .format("memory")
      .queryName(queryName)
      .option("checkpointLocation", "data\\checkpoint")
      .outputMode(OutputMode.Update())
      .start
  }
}
