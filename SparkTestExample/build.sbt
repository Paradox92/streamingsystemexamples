name := "SparkTestExample"

version := "0.1"

scalaVersion := "2.11.12"

libraryDependencies ++= Seq(
  "org.apache.spark" % "spark-sql_2.11" % "2.4.5",
  "org.apache.spark" % "spark-core_2.11" % "2.4.5",
  "com.holdenkarau" %% "spark-testing-base" % "2.4.5_0.14.0" % Test
)

parallelExecution in Test := false
fork in Test := true
javaOptions ++= Seq("-Xms512M", "-Xmx2048M", "-XX:MaxPermSize=2048M", "-XX:+CMSClassUnloadingEnabled")
